package multiplos;
public class multiplos {
	public static void main(String[] args) {
		Integer a = 0;
		while (a <= 100) {
			System.out.println(retornaMsgMultiplo(a));
			a++;
		}
	}
	private static String retornaMsgMultiplo(Integer numero) {
		String msg ="";
		if (numero % 3 == 0) {
			msg = "Foo";
		}
		if (numero % 5 == 0) {
			msg = msg+"Baa";
		}
		if (msg.isEmpty()) {
			msg = numero.toString();
		}
		return msg;
	}
}